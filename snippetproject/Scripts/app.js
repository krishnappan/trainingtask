var app=angular.module('app',[])
app.controller('ctrl',($scope,$http)=>{
   
   
    $scope.view=()=>{
     var id=$scope.id;
    var codeId=$scope.codeId;   
     console.log("Hi User"+id);
    if(codeId)
    {
        $http({
        method:'GET',
        url:'http:/localhost:3000/user/'+id+'/code/'+codeId
    }).then(function success (output){
        $scope.output=output;
        console.log('user code Successfully Retrieved!');
    },function failure (output){
        console.log(output);
        console.log("Failure!!!");
    });
    }
    else
    {
    $http({
        method:'GET',
        url:'http:/localhost:3000/user/'+id
    }).then(function success (output){
        $scope.output=output;
        console.log(' User Successfully Retrieved!');
    },function failure(output){
          console.log(output);
        console.log("Failure-retrieval!!!");
    }); 
    }
    }

    $scope.insert=()=>{
        var id=$scope.id;
     console.log("Hi User"+id);
     $http({
        method:'POST',
        url:'http:/localhost:3000/user/'+id,
        data : {
            codeId:$scope.codeId,  
            lang:$scope.lang,
            code:$scope.code, 
        }
    }).then(function success (output){
        $scope.output=output;
        console.log('Successfully Inserted!');
    },function failure (output){
        console.log("Failure --insert!!!");
    });
    }



    $scope.update=()=>{
        var id=$scope.id;
        var codeId=$scope.codeId;
     console.log("Hi User"+id);
     $http({
        method:'PUT',
        url:'http:/localhost:3000/user/'+id+'/code/'+codeId,
        data : {
            codeId:$scope.codeId,  
            lang:$scope.lang,
            code:$scope.code, 
        }
    }).then(function success (output){
        $scope.output=output;
        console.log('Successfully Updated!');
    },function failure (output){
        console.log("Failure --update!!!");
    });
    }

 $scope.remove=()=>{
        var id=$scope.id;
        var codeId=$scope.codeId;
     console.log("Hi User"+id);
     $http({
        method:'DELETE',
        url:'http:/localhost:3000/user/'+id+'/code/'+codeId
    }).then(function success (output){
        $scope.output=output;
        console.log('Successfully Delete!');
    },function failure (output){
        console.log("Failure --Delete!!!");
    });
    }

});