const r = require('rethinkdb');
const co = require('co');
const joi=require('joi');
const schema=require('./Schema/schema');
var _conn;

class DBConnect {
    constructor() {
        this.conn = _conn;
    }
    getCode(id) {
        let me = this;
        return co(function* () {
            try {
                joi.validate({id:id},schema.userdbSchema,(err,val)=>{
                    if(err)
                        throw err;
                })
                var data = yield r.table('snippet').filter(r.row('user').eq(id)).without('id','user').run(me.conn)
                let arrayPromise = data.toArray();
                return arrayPromise;
            } catch (err) {
                throw err;
            }
        });
    }
    getCodeById(id,codeId) {
        let me = this;
        return co(function* () {
            try {
                joi.validate({id:id,codeId:codeId},schema.getuserSchema,(err,val)=>{
                    if(err)
                        throw err;
                })
                var data = yield r.table('snippet').filter({'user':id,'codeId':codeId}).without('id','user').run(me.conn)
                let arrayPromise = data.toArray();
                return arrayPromise;
            } catch (err) {
                throw err;
            }
        });
    }
    addCode(id,obj){
        let me=this;
        return co(function* () {
            try {
                joi.validate({id:id},schema.userSchema,(err,val)=>{
                    if(err)
                        throw err;
                    joi.validate({codeId:obj.codeId,lang:obj.lang,code:obj.code,dateCreated:obj.dateCreated,
                        dateModified:obj.dateModified,},schema.codeSchema,(err,val)=>{
                    if(err)
                        throw err;
                })
                })
                var data = yield r.table('snippet').insert({
                    'user':id,
                    'codeId':obj.codeId,
                    'details':{
                        'code':obj.code,
                        'dateCreated':obj.dateCreated,
                        'dateModified':obj.dateModified,
                        'lang':obj.lang
                    }
                }).run(me.conn)
                //let arrayPromise = data.toArray();
                //return arrayPromise;
                return data;
            } catch (err) {
                throw err;
            }
        });
    }

    Init() {
        let me = this;
        return co(function* () {
            try {
                var conn1 = yield r.connect({ host: 'localhost', port: 28015 });
                //    if (err) throw err;
                _conn = conn1;
                //console.log(_conn);
                me.conn = conn1;

            } catch (err) {
                throw err;
            }

        });
    }
    deleteCode(id, codeId) {
        let me = this;
        return co(function* () {
            try {
            
                joi.validate({id:id,codeId:codeId},schema.getuserSchema,(err,val)=>{
                    if(err)
                        throw err;
                })
                var data = yield r.table('snippet').filter({'user':id,'codeId':codeId}).delete().run(me.conn)
               // let arrayPromise = data.toArray();
                return data;
            } catch (err) {
                throw err;
            }
        });
    }
    updateCode(id, codeId,obj) {
        let me = this;
        return co(function* () {

            try {

                joi.validate({id:id,codeId:codeId},schema.getuserSchema,(err,val)=>{
                    if(err)
                        throw err;
                    joi.validate({lang:obj.lang,code:obj.code,
                        dateModified:obj.dateModified,},schema.updateSchema,(err,val)=>{
                    if(err)
                        throw err;
                })
                })
                var data = yield r.table('snippet').filter({'user':id,'codeId':codeId}).update({
                        'details':{
                        'code':obj.code,
                        'dateModified':obj.dateModified,
                        'lang':obj.lang
                    }
                }).run(me.conn)
               // let arrayPromise = data.toArray();
                return data;
            } catch (err) {
                throw err;
            }
        });
    }


}
module.exports = DBConnect;