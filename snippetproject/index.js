
const Hapi = require('hapi');

const joi = require('joi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const fs = require('fs');
const schema = require('./Schema/schema');
const r = require('./dbAccess')


var dbconn = new r();
dbconn.Init();


var server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 3000,
    routes: { cors: true }
});



const options = {
    info: {
        'title': 'Test API Documentation',
        'version': Pack.version
    }
};
server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.start((err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Server running at:', server.info.uri);
            }
        });
    });
server.route({
    method: 'GET',
    path: '/user/{id}',
    handler: (req, reply) => {
        //Code using json files to have data was commented out
        //var data = getcode(req.params.id);  
        //display all users
         dbconn.getCode(req.params.id).then((data) => {
            reply(data);

        },(err)=>{
                reply(err+"\nGet Validation DatabaseError!User Id should be a Number!!!!")
        })
    },
    config:

    {
        tags: ['api'],
        validate: {
            params: schema.userSchema
        }
    }
});
server.route({
    method: 'GET',
    path: '/user/{id}/code/{codeId}',
    handler: (req, reply) => {
        //display code of given user & id
        //displayuser(req, reply);
        dbconn.getCodeById(req.params.id,req.params.codeId).then((data) => {
                reply(data);
        },(err)=>{
                reply(err+"\nGetCodeByID Validation DatabaseError!")
        }
        );

    },
    config:
    {
        tags: ['api'],
        validate: {
            params: schema.getuserSchema
        }
    }
});
server.route({
    method: 'POST',
    path: '/user/{id}',
    handler: (req, reply) => {
       // addcode(req, reply);
       var obj = {
        codeId: req.payload.codeId,
        lang: req.payload.lang,
        code: req.payload.code,
        dateCreated: Date(),
        dateModified: Date()
    };
        dbconn.addCode(req.params.id,obj).then((data)=>{
            console.log("Inserted Successfully\n");
         dbconn.getCode(req.params.id).then((data)=>{
            reply(data);
        },(err)=>{
                reply(err+"\nAddcode Validation DatabaseError!")
        });   
        });
},
    config:
    {
        tags: ['api'],
        validate: {
            params: schema.userSchema,
            payload: schema.codeSchema
        }
    }
});
server.route({
    method: 'DELETE',
    path: '/user/{id}/code/{codeId}',
    handler: (req, reply) => {
        // console.log("Delete");
        //deletecode(req, reply);
        var d=dbconn.deleteCode(req.params.id,req.params.codeId).then((data)=>{
            console.log("Deleted Successfully\n");
        dbconn.getCode(req.params.id).then((data)=>{
            reply(data);
        },(err)=>{
                reply(err+"\nDelete Validation DatabaseError!")
        });    
    });    
},
    config:
    {
        tags: ['api'],
        validate: {
            params: schema.getuserSchema
        }
    }
});

server.route({
    method: 'PUT',
    path: '/user/{id}/code/{codeId}',
    handler: (req, reply) => {
        //console.log("PUT");
        //updatecode(req, reply);
        var obj={
            code:req.payload.code,
            lang:req.payload.lang,
            dateModified:Date()
        };
        dbconn.updateCode(req.params.id,req.params.codeId,obj).then((data)=>{
            console.log("Updated Successfully\n");
            dbconn.getCode(req.params.id).then((data)=>{
            reply(data);
        },(err)=>{
                reply(err+"\nUpdate Validation DatabaseError!")
        });    
        });
    },
    config:
    {
        tags: ['api'],
        validate: {
            params: schema.getuserSchema,
            payload: schema.updateSchema
        }
    }
});



getcode = (id) => {
    var code = fs.readFileSync("data" + id + ".json", "utf-8");
    return (JSON.parse(code));
}

putcode = (obj, id) => {
    fs.writeFile("data" + id + ".json", JSON.stringify(obj));
}

displayuser = (req, reply) => {
    var data = getcode(req.params.id);

    var i = req.params.codeId;

    for (var k = 0; k < data.length; k++) {
        if (data[k].codeId == i)
            reply(data[k]);
    }
}

addcode = (req, reply) => {
    data = getcode(req.params.id);
    var obj = {
        codeId: req.payload.codeId,
        lang: req.payload.lang,
        code: req.payload.code,
        dateCreated: Date(),
        dateModified: Date()
    };

    data.push(obj);
    reply(data);
    putcode(data, req.params.id);

}
deletecode = (req, reply) => {
    data = getcode(req.params.id);
    var i = req.params.codeId;
    for (var k = 0; k < data.length; k++) {
        if (data[k].codeId == i)
            delete data.splice(k, 1);
    }
    reply(data);
    putcode(data, req.params.id);
}
updatecode = (req, reply) => {
    data = getcode(req.params.id);
    var i = req.params.codeId;
    for (var k = 0; k < data.length; k++) {
        if (data[k].codeId == i) {
            data[k].codeId = req.payload.codeId,
                data[k].lang = req.payload.lang,
                data[k].code = req.payload.code,
                data[k].dateModified = Date()
        }
    }
    reply(data);
}
