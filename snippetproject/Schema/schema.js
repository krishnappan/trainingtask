'use strict'
const joi = require('joi');
const codeSchema = {
    'codeId': joi.number().required(),
    'lang': joi.string().required(),
    'code': joi.string().required(),
    'dateCreated':joi.date(),
    'dateModified':joi.date()
    };
const userSchema={
    'id' : joi.number().required()
};
const getuserSchema={
    'id' : joi.number().required(),
    'codeId':joi.number().required()
};
const updateSchema={
    'lang': joi.string(),
    'code': joi.string(),
    'dateModified':joi.date()
}
const userdbSchema={
    'id' : joi.number().required()
};
module.exports={
    codeSchema,userSchema,getuserSchema,updateSchema,userdbSchema
};